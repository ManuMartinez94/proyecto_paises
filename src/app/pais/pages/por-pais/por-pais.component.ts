import { Component } from '@angular/core';
import { PaisService } from '../../services/pais.service';
import { Country } from '../../interfaces/paises.interface';

@Component({
  selector: 'app-por-pais',
  templateUrl: './por-pais.component.html',
  styles: [
    `
      li{
        cursor: pointer;
      }
    `
  ]
})
export class PorPaisComponent {

  public termino: string = '';
  public isError: boolean = false;
  public paises :Country[] = [];
  public paisesSugeridos :Country[] = [];

  
  constructor(private servicio: PaisService) { }

  public buscar( termino: string ){
    this.isError = false;
    this.termino = termino;
    this.servicio.buscarPais(termino).subscribe(res => {
      this.paises = res;
    },error => {
      this.isError = true;
    })
  }
  public sugerencias(termino: string){
    this.isError = false;
    this.termino = termino;
    this.servicio.buscarPais(termino)
      .subscribe(paises => this.paisesSugeridos = paises.splice(0,5)
        ,err => this.paisesSugeridos = [])

  }
  public buscarSugerencia(){
    this.buscar(this.termino);
    this.termino = '';
    this.paisesSugeridos = [];
  }

}
