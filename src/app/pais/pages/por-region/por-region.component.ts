import { Component } from '@angular/core';
import { Country } from '../../interfaces/paises.interface';
import { PaisService } from '../../services/pais.service';

@Component({
  selector: 'app-por-region',
  templateUrl: './por-region.component.html',
})
export class PorRegionComponent  {

  regiones: string[] = ['africa','europe','asia','americas', 'oceania'];
  regionActiva: string = 'africa';
  isError: boolean = false;
  paises: Country[] = [];
  constructor(private servicio: PaisService) { 
    this.activarRegion('africa');
  }

  activarRegion(region:string){
    this.regionActiva = region;
    this.servicio.buscarContinente(region)
    .subscribe(paises => {
      this.isError = false;
      this.paises = paises;
    }, error=>{
      console.log(error);
      this.isError = true;
    })
  }
  getClass(region:string):string{
    return (this.regionActiva == region) ? 'btn-primary' : 'btn-outline-primary';
  }

  


}
