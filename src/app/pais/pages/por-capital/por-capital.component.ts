import { Component, OnInit } from '@angular/core';
import { Country } from '../../interfaces/paises.interface';
import { PaisService } from '../../services/pais.service';

@Component({
  selector: 'app-por-capital',
  templateUrl: './por-capital.component.html',
  styleUrls: ['./por-capital.component.css']
})
export class PorCapitalComponent {

  public termino: string = '';
  public isError: boolean = false;
  public paises :Country[] = [];
  public paisesSugeridos :Country[] = [];


  
  constructor(private servicio: PaisService) { }

  public buscar( termino: string ){
    this.isError = false;
    this.termino = termino;
    this.servicio.buscarCapital(termino).subscribe(res => {
      this.paises = res;
    },error => {
      this.isError = true;
    })
  }
  public sugerencias(termino: string){
    this.isError = false;
    this.termino = termino;
    this.servicio.buscarCapital(termino)
      .subscribe(paises => this.paisesSugeridos = paises.splice(0,5)
        ,err => this.paisesSugeridos = [])

  }
  public buscarSugerencia(){
    this.buscar(this.termino);
    this.termino = '';
    this.paisesSugeridos = [];
  }
}
