import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap, tap } from 'rxjs';
import { Country } from '../../interfaces/paises.interface';
import { PaisService } from '../../services/pais.service';

@Component({
  selector: 'app-ver-pais',
  templateUrl: './ver-pais.component.html',
})
export class VerPaisComponent implements OnInit {
  public pais!: Country;
  constructor( private activatedRoute: ActivatedRoute, private servicio: PaisService ) { }

  ngOnInit(): void {
    this.activatedRoute.params
      .pipe(
        switchMap((params) => this.servicio.getPaisPorAlpha(params['id'])),
        tap(console.log)
      )
      .subscribe(pais=>{
        this.pais = pais[0];
      });
    /*this.activatedRoute.params
      .subscribe( params => {
        this.servicio.getPaisPorAlpha( params['id'] )
          .subscribe(pais=>{
            console.log(pais);
          })
      } );*/

  }
  getLanguages(idiomas: Object): string[]{
    return Object.values(idiomas);
  }


}
