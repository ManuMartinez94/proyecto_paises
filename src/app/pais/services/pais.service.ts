import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Country } from '../interfaces/paises.interface';

@Injectable({
  providedIn: 'root'
})
export class PaisService {

  private apiUrl: string = 'https://restcountries.com/v3.1';

get httpParams(){
  return new HttpParams()
  .set('fields','name,capital,population,flags,cca2');
}

  constructor(private servicio: HttpClient) { }

  buscarPais(termino: string): Observable<Country[]>{
    const url: string = `${this.apiUrl}/name/${termino}`;
    return this.servicio.get<Country[]>(url,{params: this.httpParams});
  }

  buscarCapital(termino: string): Observable<Country[]>{
    const url: string = `${this.apiUrl}/capital/${termino}`;
    return this.servicio.get<Country[]>(url,{params: this.httpParams});
  }
  buscarContinente(termino: string): Observable<Country[]>{
    const url: string = `${this.apiUrl}/region/${termino}`;
    return this.servicio.get<Country[]>(url,{params: this.httpParams});
  }

  getPaisPorAlpha(id: string): Observable<Country>{
    const url: string = `${this.apiUrl}/alpha/${id}`;
    return this.servicio.get<Country>(url);
  }



}
